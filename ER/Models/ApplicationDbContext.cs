﻿using ER.Models;
using Microsoft.EntityFrameworkCore;
public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext>
   options) : base(options)
    {
    }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Employee> Employee { get; set; }
    public DbSet<Account> Accounts { get; set; }
    public DbSet<Transaction> Transactions { get; set; }
    public DbSet<Log> Logs { get; set; }
    public DbSet<Report> Reports { get; set; }
}