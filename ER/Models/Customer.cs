﻿namespace ER.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; } 
        public string ContactAddress { get; set; }            
        public string User { get; set; }
        public string Password { get; set; }
        public List<Transaction> Transactions { get; set; }
        public List<Account> Accounts { get; set; }
    }
}
