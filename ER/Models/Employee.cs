﻿namespace ER.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAddress { get; set; }
        public string UserPassword { get; set; }
        public List <Transaction> Transaction { get; set; }
    }
}
