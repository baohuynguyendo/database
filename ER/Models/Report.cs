﻿namespace ER.Models
{
    public class Report
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public int LogId { get; set; }
        public int TransactionId { get; set; }
        public string ReportName { get; set; }
        public DateTime ReporDate { get; set; }     
        public Log? Log { get; set; }
        public Account? Account { get; set; }
        public Transaction? Transaction { get; set; }
    }
}
