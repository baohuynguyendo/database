﻿namespace ER.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int TransactionId { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }  
        public Transaction? Transaction { get; set; }
        public List<Report> Reports { get; set; }
    }
}
